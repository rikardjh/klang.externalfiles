/*

## Instructions: ##

0. Slice up the audio files and name them <instrument_name>_<note_value>_0_127.<extension>
1. Include Klang.extension.js after Klang
2. Use utils/convert.sh <directory> or anyother tool to convert any aif files to mp3 and ogg
3. After Klang.init use Klang.ext.addFiles( urls:string[], loadGroup:string ) to add external files to project
4. Use Klang.ext.createSampler( loadGroup:string ) to create a SamplePlayer and bind Klang events

Samples will only be mapped 1:1 with the notevalue in the filename

## Example ##

var OB8_Sound_2 = [];
*var startNote = 48;
var endNote   = 108;
for ( var i = startNote; i <= endNote; i++ ) {
    OB8_Sound_2.push('audio/OB8_Sound_2/OB8_Sound_2_'+i+'_0_127')
}
Klang.ext.audioLoader.addFiles( OB8_Sound_2,'test_load');
Klang.ext.audioLoader.createSampler('test_load');

Klang.load('test_load', function() { console.log('ok!'); } );

*/

( function () {
    if ( !Klang ) {
        console.error( 'Klang is missing' );
    }

    var audioSourceMap = [];

    var idCount = 1000;
    function generateIdString ( len ) {
        var seed = '';
        while ( seed.length < len ) {
            seed += '0';
        }
        idCount++;
        return 'ext_' + idCount;
    }

    function isNumber( n ) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function extractSampleAttribsFromName ( name ) {
        //check for pattern...
        var attributes = [];
        if (name.indexOf('-')>-1) {
            attributes = name.split('-');
        }else if (name.indexOf('_')>-1) {
            attributes = name.split('_');
        }
       
        var l = attributes.length;

        if(attributes[l-3] === '0') attributes[l-3] = 0;
        else attributes[l-3] = parseInt(attributes[l-3], 10) || attributes[l-3]; //parsed with decimal radix, in case we're dealing with leading zeros.
        if(attributes[l-2] === '0') attributes[l-2] = 0;
        else attributes[l-2] = parseInt(attributes[l-2], 10) || attributes[l-2];
        if(attributes[l-1] === '0') attributes[l-1] = 0;
        else attributes[l-1] = parseInt(attributes[l-1], 10) || attributes[l-1];

        var numbers = [];
        while ( l > 0) {
            if ( isNumber( attributes[ l ] ) ) {
                numbers.unshift( parseInt( attributes[l] ) );
            }
            l--
        }
        l = numbers.length;
        if( typeof(numbers[l-3]) === 'number' || typeof(numbers[l-2]) === 'number' || typeof(numbers[l-1]) === 'number')
                return {
                    root: numbers[l-3], 
                    lowVel: numbers[l-2], 
                    highVel: numbers[l-1]
                };

        return false;
    };

    Klang.ext = Klang.ext || {};

    Klang.ext.audioLoader = ( function () {

        return {
           
            /**
            *   Creates an SamplePlayer based on the supplied loadGroup.
            *   Also creates / binds two Klang events:
            *       .triggerEvent( "<loadGroup>", note, velocity - play note
            *       .triggerEvent( "<loadGroup_stop", note ) - stops 
            *
            *   @param {string} loadGroup The name of the loadgroup to base sampler on
            */
            createSampler : function ( loadGroup ) {
                var core        = Klang.getCoreInstance();
                var samplerId   = generateIdString( 4 );
                var samples     = [];

                for ( var i = 0, len = audioSourceMap.length; i < len; i++ ) {
                    var obj = audioSourceMap[ i ];
                    if ( obj.loadGroup === loadGroup ) {
                        var sampleAttribs = extractSampleAttribsFromName( obj.fileInfo.url ) || {};
                  
                        sampleAttribs.startRange = sampleAttribs.startRange || sampleAttribs.root;
                        sampleAttribs.endRange   = sampleAttribs.endRange   || sampleAttribs.root;
                        sampleAttribs.root       = sampleAttribs.root       || 48;

                        sampleAttribs.source = obj.sourceId;
                        samples.push( sampleAttribs );
                    }
                }

                var samplerConfig = {
                    content: [{
                        highVelocity: 127,
                        lowVelocity: 0,
                        samples: samples,
                        value: "noteOn"
                    }],

                    destination_name: core._masterBusId,

                    eg_gain: { 
                        attack: 0,
                        decay: 0,
                        release: 0.1,
                        sustain: 1
                    },

                    input_vol: 1,
                    type: "SamplePlayer",
                    volume: 1,
                    volume_curve: "exponential"
                };


                var stopProcessId = generateIdString( 4 );
                var playProcessId = generateIdString( 4 );
                var playProcessData = {
                        action: "me." + samplerId + ".handleMidiEvent({type:'channel', subtype:'noteOn',noteNumber:args[0], velocity:args[1]},Util.now()+0);",
                        type: "SimpleProcess",
                        vars: [ samplerId ]
                    };
                var stopProcessData = {
                        action: "me." + samplerId + ".handleMidiEvent({type:'channel', subtype:'noteOff',noteNumber:args[0]},Util.now()+0);",
                        type: "SimpleProcess",
                        vars: [ samplerId ]
                    };
                var sampler = core.createObject( samplerId, samplerConfig );

                core.createEvent( loadGroup, playProcessId );
                core.createObject( playProcessId, playProcessData );
                core.createEvent( loadGroup + '_stop', stopProcessId );
                core.createObject( stopProcessId, stopProcessData );

                return sampler;
            },

            /**
            *   Add external audio files to project at runtime and create AudioSources for each
            *   @param {string[]} urlList Array of URL:s to load (these need to be CORS compatible)
            *   @param {string} loadGroup='auto' The load group to use for the files
            */
            addFiles : function ( urlList, loadGroup ) {

                var core = Klang.getCoreInstance();

                loadGroup = loadGroup || 'auto';

                if ( typeof urlList === 'string' ) {

                    urlList = [ urlList ];

                }

                var fileHandler = Klang.getFileHandlerInstance();

                for ( var i = 0; i < urlList.length; i++ ) {
                    var fileUrl = urlList[ i ];
                    var fileId      = ( 1000 + Math.floor( Math.random() * 8999 ) ) + '';
                    var fileInfo    = {
                        file_type: 'audio',
                        id: fileId,
                        load_group: loadGroup,
                        url: fileUrl,
                        external: true
                    };

                    fileHandler.prepareFile( fileInfo );

                    var audioSourceId = generateIdString( 4 );

                    var audioSource = core.createObject( audioSourceId, {
                        destination_name: "$OUT",
                        duration: undefined,
                        file_id: fileId,
                        granular: undefined,
                        lock_playback_rate: undefined,
                        loop: undefined,
                        loop_end: undefined,
                        loop_start: undefined,
                        offset: undefined,
                        panner: undefined,
                        pitch_end_range: undefined,
                        pitch_start_range: undefined,
                        playback_rate: undefined,
                        retrig: true,
                        reverse: undefined,
                        type: "AudioSource",
                        volume: undefined,
                        volume_end_range: undefined,
                        volume_start_range: undefined,
                        xfade: undefined
                    } );

                    audioSourceMap.push({
                        source: audioSource,
                        sourceId: audioSourceId,
                        fileInfo: fileInfo,
                        loadGroup: loadGroup 
                    });

                }
            }
        };

    } ());


} ());