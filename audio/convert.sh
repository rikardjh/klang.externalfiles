#!/bin/bash
for f in "$1"/*.aiff
do
./ffmpeg -i "${f}" -f mp3 -acodec libmp3lame -ab 96000 -ar 44100 "${f%.*}.mp3"
./ffmpeg -i "${f}" -f ogg -acodec libvorbis -bit_rate 96000 -ar 44100 "${f%.*}.ogg" 
#ffmpeg -i  -acodec libvorbis -ab 96000 -ar 44100 
done