## Instructions: ##

0. Slice up the audio files and name them <instrument_name>_<note_value>_0_127.<extension>
1. Include Klang.extension.js after Klang
2. Use utils/convert.sh <directory> or anyother tool to convert any wav / aif files to mp3 and ogg
3. After Klang.init use Klang.ext.addFiles( urls:string[], loadGroup:string ) to add external files to project
4. Use Klang.ext.createSampler( loadGroup:string ) to create a SamplePlayer and bind Klang events

Samples will only be mapped 1:1 with the notevalue in the filename

## Example ##

var OB8_Sound_2 = [];
*var startNote = 48;
var endNote   = 108;
for ( var i = startNote; i <= endNote; i++ ) {
    OB8_Sound_2.push('audio/OB8_Sound_2/OB8_Sound_2_'+i+'_0_127')
}
Klang.ext.audioLoader.addFiles( OB8_Sound_2,'test_load');
Klang.ext.audioLoader.createSampler('test_load');

Klang.load('test_load', function() { console.log('ok!'); } );

## Convert Example ##
(tried on Mac OS)
:audio> ./convert.sh OB8_Sound_2